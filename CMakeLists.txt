cmake_minimum_required(VERSION 2.8.3)
project(video_bag_converter)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  sensor_msgs
  image_transport
  camera_info_manager
  cv_bridge
  rosbag
  tf
)

if(CMAKE_COMPILER_IS_GNUCC)
    ADD_DEFINITIONS ( -std=c++11 )
endif(CMAKE_COMPILER_IS_GNUCC)

find_package(OpenCV REQUIRED)

catkin_package(
  CATKIN_DEPENDS roscpp sensor_msgs image_transport cv_bridge rosbag tf
)

###########
## Build ##
###########
include_directories(
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

## Declare a cpp executable
add_executable(video_bag_converter_node src/video_bag_converter_node.cpp)

## Specify libraries to link a library or executable target against
target_link_libraries(video_bag_converter_node
  ${catkin_LIBRARIES} ${OpenCV_LIBRARIES}
)

## Declare a cpp executable
add_executable(extract_images_node src/extract_images.cpp)

## Specify libraries to link a library or executable target against
target_link_libraries(extract_images_node
  ${catkin_LIBRARIES} ${OpenCV_LIBRARIES}
)

add_executable(topic_video_converter_node src/topic_video_converter_node.cpp)
target_link_libraries(topic_video_converter_node
  ${catkin_LIBRARIES} ${OpenCV_LIBRARIES}
)
