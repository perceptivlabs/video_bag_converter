#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <sstream>

// ROS
#include <exception>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <message_filters/subscriber.h>
#include <image_transport/subscriber_filter.h>
#include <opencv2/opencv.hpp>


typedef image_transport::SubscriberFilter ImageSubscriber;


class TopicVideoConverter
{
private:
  ros::NodeHandle nh_;
  ros::Subscriber image_sub_;
  image_transport::ImageTransport it_;
  int fps_;
  std::string file_name_;
  cv::Mat frame_;
  cv::VideoWriter output_;
  bool init_;

public:
  void imgCallback(const sensor_msgs::ImageConstPtr& image)
  {
    cv_bridge::CvImagePtr img_cvptr;
    try
    {
      img_cvptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);
    } 
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    frame_ = img_cvptr->image;

    if (!init_)
    {
      output_.open(file_name_, CV_FOURCC('X','V','I','D'), fps_, frame_.size(), true);
      if (!output_.isOpened())
      {
        ROS_ERROR( "Could not open the output video for write: %s", file_name_.c_str());
        throw std::invalid_argument("Could not open the output video for write");
      }
      init_ = true;
    }
    output_.write(frame_); 
  }

  TopicVideoConverter():it_(nh_), init_(false)
  {
    ros::param::param<int>("~fps", fps_, 60);
    ros::param::param<std::string>("~file_name", file_name_, "/tmp/video.avi");
    image_sub_ = nh_.subscribe("image_raw", 100, &TopicVideoConverter::imgCallback, this);
  }
};

int main(int argc, char** argv)
{
  // Initialize ROS
  ros::init(argc, argv, "topic_video_converter");

  // Convert Image
  TopicVideoConverter ic;
  ros::spin();

  return 0;
}
