#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <sstream>

// ROS
#include <exception>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <message_filters/subscriber.h>
#include <image_transport/subscriber_filter.h>
#include <camera_info_manager/camera_info_manager.h>
#include <opencv2/opencv.hpp>
#include <rosbag/bag.h>


typedef image_transport::SubscriberFilter ImageSubscriber;


class VideoBagConverter
{
private:
  ros::NodeHandle nh_;
  ros::Publisher image_pub_;
  ros::Publisher camera_info_pub_;
  ros::Timer image_read_timer_;
  rosbag::Bag out_bag_;
  image_transport::ImageTransport it_;
  std::shared_ptr<camera_info_manager::CameraInfoManager> camera_info_manager_;
  sensor_msgs::CameraInfoPtr camera_info_;
  cv::VideoCapture cap_;
  int width_, height_, fps_;
  std::string file_name_;
  std::string camera_info_url_;
  double rescale_;
  cv::Mat frame_;
  int img_idx_;

public:
  void imgPubCallback(const ros::TimerEvent&)
  {
    bool success = cap_.read(frame_); // read a new frame from video
    if(!success)
    {
      ROS_ERROR("Can't read frames!");
      return;
    }

    cv::resize(frame_, frame_, cv::Size(0,0), rescale_, rescale_);

    cv_bridge::CvImage img;
    img.image = frame_;
    sensor_msgs::Image img_msg;
    img.toImageMsg(img_msg);
    img_msg.encoding = "bgr8";
    img_msg.header.seq = img_idx_++;
    img_msg.header.stamp = ros::Time::now();
    image_pub_.publish(img_msg);

    out_bag_.write("/camera/image_raw", img_msg.header.stamp, img_msg);
    ROS_INFO("wrote frame!");

    if (!camera_info_url_.empty())
    {
      out_bag_.write("/camera/camera_info", img_msg.header.stamp, *camera_info_);
      camera_info_pub_.publish(*camera_info_);
      ROS_INFO("wrote camera info!");
    }
  }

  VideoBagConverter():it_(nh_), img_idx_(0),
    camera_info_manager_(new camera_info_manager::CameraInfoManager(nh_))
  {
    std::string bag_name;
    ros::param::param<int>("~fps", fps_, 10);
    ros::param::param<double>("~rescale", rescale_, 1.0);
    ros::param::param<std::string>("~file_name", file_name_, "/tmp/video.mpeg");
    ros::param::param<std::string>("~bag_name", bag_name, "/tmp/video.bag");
    ros::param::param<std::string>("~camera_info_url", camera_info_url_, "");
    image_pub_ = nh_.advertise<sensor_msgs::Image>("image_raw", 100);
    camera_info_pub_ = nh_.advertise<sensor_msgs::CameraInfo>("camera_info", 100);
    image_read_timer_ = nh_.createTimer(ros::Duration(1/(double)fps_),
      &VideoBagConverter::imgPubCallback, this);

    if (!camera_info_url_.empty())
    {
      ROS_INFO_STREAM("Loading calib file" << camera_info_url_);
      if (camera_info_manager_->validateURL(camera_info_url_))
      {
        camera_info_manager_->loadCameraInfo(camera_info_url_);
      } else {
        ROS_ERROR("invalid camera calib file");
        throw std::runtime_error("invalid camera calib file");
        return;
      }

      camera_info_.reset(new sensor_msgs::CameraInfo(camera_info_manager_->getCameraInfo()));
      camera_info_->width *= rescale_;
      camera_info_->height *= rescale_;
      camera_info_->K[0] *= rescale_;
      camera_info_->K[2] *= rescale_;
      camera_info_->K[4] *= rescale_;
      camera_info_->K[5] *= rescale_;
    }
    cap_.open(file_name_);
    out_bag_.open(bag_name, rosbag::bagmode::Write);

    if(!cap_.isOpened())
    {
      throw std::invalid_argument("invalid file name");
    }
  }
};

int main(int argc, char** argv)
{
  // Initialize ROS
  ros::init(argc, argv, "video_bag_converter");

  // Convert Image
  VideoBagConverter ic;
  ros::spin();
  return 0;
}
